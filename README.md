# Speqtiv

## Table of Contents
1. [Credentials](#markdown-header-credentials)
1. [System Setup](#markdown-header-system-setup)
1. [Database Setup](#markdown-header-database-setup)
1. [Connecting to Database from Sequel Pro](#markdown-header-connecting-to-database-from-sequel-pro)

---

## Credentials

**Spark API Key** - aiDl9rQAE8BAKXMPloKpOcBoPQWfvxlVgs5TZlNznTuY88y9PEpXJ6kOGPXv


---

## System Setup
1. [Install Vagrant](https://www.vagrantup.com/downloads.html)
2. [Install Virtual Box](https://www.virtualbox.org/wiki/Downloads)
1. [Install Composer](https://getcomposer.org/download/), run the commands found on that page in your console to install.
1. Make Composer available globally:

        mv composer.phar /usr/local/bin/composer

1. Then install laravel:

        composer global require "laravel/installer"

1. Add composer to your path:

        sudo nano ~/.bashrc

    add this to the file and save: 

        export PATH="$HOME/.composer/vendor/bin:$PATH"


1. Add the following to your /etc/hosts file:

        192.168.10.10   speqtiv.test

1. In terminal navigate to your repository's directory and run:

        cd speqtiv
        npm install
        composer install
        vagrant up

1. Once completed, go to [http://speqtiv.test](http://speqtiv.test), expect to see a styled error page, since you don't have a database set up yet. But as long as you see the message "Whoops, looks like something went wrong." You are most likely on the right track so far!

---

## Database Setup
1. SSH into your vagrant box
        
        vagrant ssh

1. navigate to your code folder
       
        cd code

1. run the database migration command

        php artisan migrate

When this runs, it will create a database (spectiv), and all of the tables required to run the application.

Note: You'll have to run this command every time that a database change is made (either through a repo pull that includes database migrations or if you make changes to the database configuration files yourself).

---

## Connecting to Database from Sequel Pro
When creating a new connection to your database use the following settings:
Connect with ssh

    Name: Speqtiv
    MySQL Host: 192.158.10.10
    Username: root
    Password: secret
    Database: speqtiv
    Port: 3306
    SSH Host: 192.158.10.10
    SSH User: vagrant
    SSH Key: ~/.ssh/id_rsa